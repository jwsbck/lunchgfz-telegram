import abc
import logging
import sys
from typing import Union

from telethon import TelegramClient

from lunchgfz.common import FAULTY_ARGUMENTS_EXITCODE


class BaseMenu(metaclass=abc.ABCMeta):

    def __init__(self, abbr: str, name: str, icon: str, publish: bool) -> None:
        self.abbr = abbr
        self.name = name
        self.icon = icon
        self.telethon_client: TelegramClient = None
        self.publish = publish


class IPPBase(BaseMenu, metaclass=abc.ABCMeta):

    def __init__(self, publish: bool) -> None:
        super().__init__(publish=publish,
                         abbr="IPP",
                         name="Canteen Konradhof Catering at the Max-Planck-Institut für Plasmaphysik (IPP)",
                         icon="⚛")


class FMIBase(BaseMenu, metaclass=abc.ABCMeta):

    def __init__(self, publish: bool) -> None:
        super().__init__(publish=publish,
                         abbr="FMI",
                         name="Canteen Wilhelm Gastronomie inside the Department of Mathematics and Informatics (FMI)",
                         icon="💻")


class SWMBase(BaseMenu, metaclass=abc.ABCMeta):

    def __init__(self, publish: bool, location: Union[int, str]) -> None:
        log = logging.getLogger("SWMBase")

        try:
            location_id = int(location)
        except ValueError:
            try:
                location_id = self.location_id_mapping[location]
            except KeyError:
                log.critical("Location %s not found.", location)
                print("Location {} not found. Choose one of {}.".format(
                    location, ', '.join(self.location_id_mapping.keys())), file=sys.stderr)
                exit(FAULTY_ARGUMENTS_EXITCODE)

        self.location_id = location_id

        super().__init__(publish=publish,
                         abbr=self.location_abbr_mapping[location_id],
                         name="Studentenwerk München canteen, ID: {}, short name: {}".format(
                             location_id, self.location_id_mapping_inverse[location_id]),
                         icon=self.location_icon_mapping[location_id])

    # Some of the locations do not use the general Studentenwerk system and do not have a location id.
    # It differs how they publish their menus — probably everyone needs an own parser.
    # For documentation they are in the list but commented out.
    location_id_mapping = {
        "mensa-arcisstr": 421,
        "mensa-garching": 422,
        "mensa-leopoldstr": 411,
        "mensa-lothstr": 431,
        "mensa-martinsried": 412,
        "mensa-pasing": 432,
        "mensa-weihenstephan": 423,
        "stubistro-arcisstr": 450,
        # "stubistro-benediktbeuern": ,
        "stubistro-goethestr": 418,
        "stubistro-großhadern": 414,
        "stubistro-grosshadern": 414,
        "stubistro-rosenheim": 441,
        "stubistro-schellingstr": 416,
        # "stubistro-schillerstr": ,
        "stucafe-adalbertstr": 512,
        "stucafe-akademie-weihenstephan": 526,
        # "stucafe-audimax" ,
        "stucafe-boltzmannstr": 527,
        "stucafe-garching": 524,
        # "stucafe-heßstr": ,
        "stucafe-karlstr": 532,
        # "stucafe-leopoldstr": ,
        # "stucafe-olympiapark": ,
        "stucafe-pasing": 534,
        # "stucafe-weihenstephan": ,
    }

    location_id_mapping_inverse = {v: k for k, v in location_id_mapping.items()}

    location_abbr_mapping = {
        421: "mensa-arcisstr",  # "mensa-arcisstr"
        422: "MG",  # "mensa-garching"
        411: "mensa-leopoldstr",  # "mensa-leopoldstr"
        431: "mensa-lothstr",  # "mensa-lothstr"
        412: "mensa-martinsried",  # "mensa-martinsried"
        432: "mensa-pasing",  # "mensa-pasing"
        423: "mensa-weihenstephan",  # "mensa-weihenstephan"
        450: "stubistro-arcisstr",  # "stubistro-arcisstr"
        # "stubistro-benediktbeuern": ,  # "stubistro-benediktbeuern"
        418: "stubistro-goethestr",  # "stubistro-goethestr"
        414: "stubistro-großhadern",  # "stubistro-großhadern"
        441: "stubistro-rosenheim",  # "stubistro-rosenheim"
        416: "stubistro-schellingstr",  # "stubistro-schellingstr"
        # "stubistro-schillerstr": ,  # "stubistro-schillerstr"
        512: "stucafe-adalbertstr",  # "stucafe-adalbertstr"
        526: "stucafe-akademie-weihenstephan",  # "stucafe-akademie-weihenstephan"
        # "stucafe-audimax": ,  # "stucafe-audimax",
        527: "MW",  # "stucafe-boltzmannstr"
        524: "SCG",  # "stucafe-garching"
        # "stucafe-heßstr": ,  # "stucafe-heßstr",
        532: "stucafe-karlstr",  # "stucafe-karlstr"
        # "stucafe-leopoldstr": ,  # "stucafe-leopoldstr"
        # "stucafe-olympiapark": ,  # "stucafe-olympiapark"
        534: "stucafe-pasing",  # "stucafe-pasing"
        # "stucafe-weihenstephan": ,  # "stucafe-weihenstephan"
    }

    location_icon_mapping = {
        421: "◼",  # "mensa-arcisstr"
        422: "Ⓜ",  # "mensa-garching"
        411: "◼",  # "mensa-leopoldstr"
        431: "◼",  # "mensa-lothstr"
        412: "◼",  # "mensa-martinsried"
        432: "◼",  # "mensa-pasing"
        423: "◼",  # "mensa-weihenstephan"
        450: "◼",  # "stubistro-arcisstr"
        # "stubistro-benediktbeuern": ,  # "stubistro-benediktbeuern"
        418: "◼",  # "stubistro-goethestr"
        414: "◼",  # "stubistro-großhadern"
        441: "◼",  # "stubistro-rosenheim"
        416: "◼",  # "stubistro-schellingstr"
        # "stubistro-schillerstr": ,  # "stubistro-schillerstr"
        512: "◼",  # "stucafe-adalbertstr"
        526: "◼",  # "stucafe-akademie-weihenstephan"
        # "stucafe-audimax": ,  # "stucafe-audimax",
        527: "🔩",  # "stucafe-boltzmannstr"
        524: "☕",  # "stucafe-garching"
        # "stucafe-heßstr": ,  # "stucafe-heßstr",
        532: "◼",  # "stucafe-karlstr"
        # "stucafe-leopoldstr": ,  # "stucafe-leopoldstr"
        # "stucafe-olympiapark": ,  # "stucafe-olympiapark"
        534: "◼",  # "stucafe-pasing"
        # "stucafe-weihenstephan": ,  # "stucafe-weihenstephan"
    }
