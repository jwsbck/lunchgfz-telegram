INGREDIENTS_EMOJI = {
    # ATTENTION: You need a emoji compatible font to display the values of this dict or else they are invisible.
    # In this case do not modify the dict values to not accidientially delete the emojis.
    "GQB": '🏅',  # "Certified Quality - Bavaria",
    "MSC": '🌊',  # "Marine Stewardship Council",

    "1": '🎨',  # "with dyestuff",
    "2": '➿',  # '♾','❄️','➿' "with preservative",
    "3": '🌬️',  # '🌀' "with antioxidant",
    "4": '😋',  # "with flavor enhancers",
    "5": '🜍',  # "sulphured",
    "6": '⬛',  # "blackened (olive)",
    "7": '🕯',  # "waxed",
    "8": '🦴',  # "with phosphate",
    "9": '🍬',  # "with sweeteners",
    "10": '🍼',  # "contains a source of phenylalanine",
    "11": '🍭',  # "with sugar and sweeteners",
    "13": '🍫',  # "with cocoa-containing grease",
    "14": '🍮',  # "with gelatin",
    "99": '🍸',  # "with alcohol",

    "f": '🧀',  # "meatless dish",
    "v": '🍏',  # "vegan dish",
    "S": '🐖',  # "with pork",
    "R": '🐂',  # "with beef",
    "K": '🐮',  # "with veal",
    "G": '🐓',  # "with poultry", # mediziner mensa
    "W": '🐗',  # "with wild meat", # mediziner mensa
    "L": '🐑',  # "with lamb", # mediziner mensa
    "Kn": '🥘',  # '🧄' (emoji version 12.0 in 2019) "with garlic",
    "Ei": '🥚',  # "with chicken egg",
    "En": '🥜',  # "with peanut",
    "Fi": '🐟',  # "with fish",
    "Gl": '🥣',  # "with gluten-containing cereals",
    "GlW": '🌾',  # "with wheat",
    "GlR": '',  # "with rye",
    "GlG": '',  # "with barley",
    "GlH": '',  # "with oats",
    "GlD": '',  # "with spelt",
    "Kr": '🦀',  # "with crustaceans",
    "Lu": '🐺',  # "with lupines",
    "Mi": '🥛',  # "with milk and lactose",
    "Sc": '🌰',  # "with shell fruits",
    "ScM": '',  # "with almonds",
    "ScH": '',  # "with hazelnuts",
    "ScW": '',  # "with Walnuts",
    "ScC": '',  # "with cashew nuts",
    "ScP": '',  # "with pistachios",
    "Se": '🌱',  # "with sesame seeds",
    "Sf": '🌭',  # '🌭' "with mustard",
    "Sl": '🌿',  # '🌿'(herb/Kräuter) '🌱'(Seeding) "with celery",
    "So": '🍢',  # '🍢'(Oden often served in soy) "with soy",
    "Sw": '',  # "with sulfur dioxide and sulfites",
    "Wt": '🐌',  # "with mollusks",
}
"""A dict mapping for ingredients codes to emojis. Empty mappings are possible."""


INGREDIENTS_GERMAN = {
    # ATTENTION: You need a emoji compatible font to display the values of this dict or else they are invisible.
    # In this case do not modify the dict values to not accidientially delete the emojis.
    "GQB": "Zertifizierte Qualität - Bayern",  # "Certified Quality - Bavaria",
    "MSC": "Marine Stewardship Council",  # "Marine Stewardship Council",

    "1": "Farbstoffe",  # "with dyestuff",
    "2": "Konservierungsmittel",  # ❄️➿ "with preservative",
    "3": "Antioxidationsmittel",  # "with antioxidant",
    "4": "Geschmacksverstärker",  # "with flavor enhancers",
    "5": "Sulphate",  # "sulphured",
    "6": "geschwärzt (Olive)",  # "blackened (olive)",
    "7": "gewachst",  # "waxed",
    "8": "Phosphate",  # "with phosphate",
    "9": "Süßungsmittel",  # "with sweeteners",
    "10": "Phenylalanin",  # "contains a source of phenylalanine",
    "11": "Zucker und Süßungsmittel",  # "with sugar and sweeteners",
    "13": "Fett mit Kakao",  # "with cocoa-containing grease",
    "14": "Gelantine",  # "with gelatin",
    "99": "Alkohol",  # "with alcohol",

    "f": "fleischlos",  # "meatless dish",
    "v": "vegan",  # "vegan dish",
    "S": "Schweinefleisch",  # "with pork",
    "R": "Rind",  # "with beef",
    "K": "Kalb",  # "with veal",
    "G": "Geflügel",  # "with poultry", # mediziner mensa
    "W": "Wildfleish",  # "with wild meat", # mediziner mensa
    "L": "Lamm",  # "with lamb", # mediziner mensa
    "Kn": "Knoblauch",  # "with garlic",
    "Ei": "Hühnerei",  # "with chicken egg",
    "En": "Erdnuss",  # "with peanut",
    "Fi": "Fisch",  # "with fish",
    "Gl": "Getreide mit Gluten",  # "with gluten-containing cereals",
    "GlW": "Weizen",  # "with wheat",
    "GlR": "Roggen",  # "with rye",
    "GlG": "Gerste",  # "with barley",
    "GlH": "Hafer",  # "with oats",
    "GlD": "Spelz",  # "with spelt",
    "Kr": "Krustentiere",  # "with crustaceans",
    "Lu": "Lupine",  # "with lupines",
    "Mi": "Milch und Laktose",  # "with milk and lactose",
    "Sc": "Schalenfrüchte",  # "with shell fruits",
    "ScM": "Mandeln",  # "with almonds",
    "ScH": "Haselnüsse",  # "with hazelnuts",
    "ScW": "Walsüssen",  # "with Walnuts",
    "ScC": "Cashewnüssen",  # "with cashew nuts",
    "ScP": "Pistazien",  # "with pistachios",
    "Se": "Sesamsamen",  # "with sesame seeds",
    "Sf": "Senf",  # "with mustard",
    "Sl": "Sellerie",  # "with celery",
    "So": "Soja",  # "with soy",
    "Sw": "Schwefeldioxide und Sulfite",  # "with sulfur dioxide and sulfites",
    "Wt": "Weichtiere",  # "with mollusks",
}
"""A dict mapping for ingredients codes to german description."""
