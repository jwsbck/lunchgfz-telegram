import datetime as dt
import logging
import tempfile
import time
from pathlib import Path
from typing import Iterable

from telethon import TelegramClient
from telethon.errors import SessionPasswordNeededError

from lunchgfz.secrets_conf import API_HASH, API_ID, PASSWORD_2FA, PHONE_NUMBER  # pylint: disable=import-error

TELEGRAM_RETRY_TRIES = 10
TELEGRAM_RETRY_SLEEP = 150  # in seconds

CONNECTION_FAILED_EXITCODE = 2
FAULTY_ARGUMENTS_EXITCODE = 3
DAY_WITHOUT_MENU_EXITCODE = 4
DOWNLOAD_FAILED_EXITCODE = 5
CHANNEL_NAME = "lunchgfz"

Automatic = None  # pylint: disable=invalid-name

_log = logging.getLogger()  # pylint: disable=invalid-name

tempdir = Path(tempfile.gettempdir())  # pylint: disable=invalid-name

default_cache_dir = Path(tempdir, "lunchgfz/")  # pylint: disable=invalid-name

default_cache_dir.mkdir(parents=True, exist_ok=True)


class DayWithoutMenuException(Exception):
    pass


class AlreadyPublishedException(Exception):
    pass


class DayWithoutMenuWarning(Warning):
    pass


def atleast(iterable: Iterable, times: int) -> bool:
    """Returns true if at least times elements in iterable are true."""
    # https://stackoverflow.com/a/42514511

    iterator = iter(iterable)
    return all(any(iterator) for _ in range(times))


def next_weekday(date: dt.date, weekday: int) -> dt.date:
    """Returns the date of next weekday specified as parameter."""
    days_ahead = weekday - date.weekday()
    if days_ahead <= 0:  # Target day already happened this week
        days_ahead += 7
    return date + dt.timedelta(days_ahead)


def get_monday(date: dt.date) -> dt.date:
    """
    If it is weekend return monday of next week else monday of current week
    """
    if date.weekday() >= 5:  # sa or sun
        return date + dt.timedelta(7 - date.weekday())

    return date - dt.timedelta(date.weekday())


__global_client = None  # pylint: disable=invalid-name
"""A global client instance so that we do not instantiate multiple clients with batch runs."""


def client_connect() -> TelegramClient:
    """Creates a Telegram client and connect it. Returns always the same client if he is still connected."""
    global __global_client  # pylint: disable=invalid-name

    if __global_client and __global_client.is_connected():
        return __global_client

    client = TelegramClient('lunchgfz', API_ID, API_HASH)

    _log.debug("Connect to telegram server.")

    for i in range(TELEGRAM_RETRY_TRIES):
        if client.connect():
            break
        _log.error("Connection try no.%d failed. Wait for %dsec.", i, TELEGRAM_RETRY_SLEEP)
        time.sleep(TELEGRAM_RETRY_SLEEP)
    else:
        _log.critical("All connection tries failed. Exit application.")
        exit(CONNECTION_FAILED_EXITCODE)

    if client.is_user_authorized():
        _log.info("Client successfully authenticated.")
    else:
        _log.warning("Send code request.")
        client.sign_in(PHONE_NUMBER)
        try:
            client.sign_in(code=input('Enter one-time code: '))
        except SessionPasswordNeededError:
            client.sign_in(password=PASSWORD_2FA)

        myself = client.get_me()
        _log.warning(myself.stringify())

    __global_client = client
    return client


def client_disconnect() -> None:
    """Disconnects the client."""
    if __global_client and __global_client.is_connected():
        __global_client.disconnect()
