🚧 Bot is still in development!

A friendly bot which posts the lunch menu of canteens located at the campus Garching Forschungszentrum (GFZ) of the Technical University of Munich (TUM) to this Telegram group here (http://t.me/lunchgfz).

💬 Mondays at 10:00 o‘clock the bot posts the weekly lunch menu as multiple pictures for every canteen. Additionally, from Monday till Friday and every morning at 10:30 o‘clock, the bot posts the lunch menu of the current day as text.

Currently the following canteens are supported:
  ☕️☢️SCG • StuCafé inside the Mensa Garching
  🔩☕️MW • StuCafé Boltzmannstraße inside the Department of Mechanical Engineering (Maschinenwesen)
  Ⓜ️☢️MG • Mensa Garching
      http://www.studentenwerk-muenchen.de/mensa/standorte-und-oeffnungszeiten/garching/
  💻FMI • Canteen Wilhelm Gastronomie inside the Department of Mathematics and Informatics (FMI)
      http://www.wilhelm-gastronomie.de/tum-garching
  ⚛️IPP •  Canteen Konradhof Catering at the Max-Planck-Institut für Plasmaphysik (IPP)
      https://konradhof-catering.de/ipp/

Legend:
  Ⓜ️ = Mensa of Studentenwerk München
  ☕️ = StuCafé of Studentenwerk München
  ☢️ = Garching
  🍏 = Vegan dish
  🧀 = Vegetarian dish
  🍗 = Dish with meat or fish
  🌍 = International dish
  🎎 = Traditional dish
  💥 = Special dish

⚠️ I do not guarantee the correctness of the bot or the posted content (Irrtümer vorbehalten).

The bot‘s project homepage is: 
  https://gitlab.com/raabf/lunchgfz-telegram
For parsing the PDFs, the bot makes use of:
  https://github.com/srehwald/eat-api/

🛑 Problems should go to the issue tracker of the respective project homepage.